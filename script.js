// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.
const address = ["258", "Washington Ave NW", "California", "90011"]

// Destructure the array and print out a message with the full address using Template Literals.
const [houseNumber, street, state, zip] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zip}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
	name: "Lolong",
	crocType: "saltwater crocodile",
	weight: "1075 kgs", 
	measure: "20 ft 3 in"
}
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const {name, crocType, weight, measure} = animal;
console.log(`${name} was a ${crocType}. He weighed at ${weight} with a measurment of ${measure}.`);

// 9. Create an array of numbers.
const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(`${number}`));

const reduceNumber = numbers.reduce((x,y) => x + y);
console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myPet = new Dog();
myPet.breed = "Beagle";
myPet.age = "5";
myPet.name = "Billy";

console.log(myPet);